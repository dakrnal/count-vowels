﻿using System;

namespace VowelCountTask
{
    public static class StringHelper
    {
        /// <summary>
        /// Calculates the count of vowels in the source string.
        ///  'a', 'e', 'i', 'o', and 'u' are vowels.
        /// </summary>
        /// <param name="source">Source string.</param>
        /// <returns>Count of vowels in the given string.</returns>
        /// <exception cref="ArgumentException">Thrown when source string is null or empty.</exception>
        public static int GetCountOfVowel(string source)
        {
            if (source == null || source.Length <= 0)
            {
                throw new ArgumentException("String cannot be null or empty.");
            }

            List<char> vletters = new List<char> { 'a', 'e', 'i', 'o', 'u' };
            List<char> fletters = source.Where(x => char.IsLetter(x) && vletters.Contains(x)).ToList();
            return fletters.Count;
        }
    }
}
